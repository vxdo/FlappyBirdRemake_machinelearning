class Bird {
    constructor(canvas, ctx, color) {
        this.x = 50;
        this.y = Math.floor(Math.random() * canvas.height);
        this.r = 20;
        this.canvas = canvas;
        this.ctx = ctx;
        this.color = color;
        this.downSpeed = .5;
        this.upSpeed = -2;
        this.flapping = false;
        this.dy = 0;
        this.maxFlap = 0;
    }

    get X() {
        return this.x;
    }

    get Y() {
        return this.y;
    }

    get R() {
        return this.r;
    }

    setY(y) {
        this.y = y;
    }

    draw() {
        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
        this.ctx.fillStyle = this.color;
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();
    }

    isTouched(rect) {
        let circle = this;

        let distX = Math.abs(circle.X - rect.X - rect.W / 2);
        let distY = Math.abs(circle.Y - rect.Y - rect.H / 2);
        let distY2 = Math.abs(circle.Y - rect.Y2 - rect.H2 / 2);
        // console.log('bird: ', distX > (rect.W / 2 + circle.R) && (distY > (rect.H / 2 + circle.R) || distY2 > (rect.H2 / 2 + circle.R)))
        if (distX > (rect.W / 2 + circle.R) && (distY > (rect.H / 2 + circle.R) || distY2 > (rect.H2 / 2 + circle.R))) {
            return 'm';
        }
        // if (distY > (rect.H / 2 + circle.R)) {
        //     return false;
        // }
        // if (distY2 > (rect.H2 / 2 + circle.R)) {
        //     return false;
        // }

        if (distX <= (rect.W / 2)) {
            if (distY <= (rect.H / 2)) {
                return 'u';
            } else if (distY2 <= (rect.H2 / 2)) {
                return 'l';
            }
        }

        let dx = distX - rect.W / 2;
        let dy = distY - rect.H / 2;
        let dy2 = distY2 - rect.H2 / 2;

        return (dx * dx + dy * dy <= (circle.R * circle.R)) ? 'u' : (dx * dx + dy2 * dy2 <= (circle.R * circle.R)) ? 'l' : '';
    }

    update() {
        if (this.flapping && this.maxFlap <= this.y && this.y > 0) {
            this.upSpeed = -2;
        } else {
            this.upSpeed = 0;
            this.flapping = false
        }

        this.dy += this.downSpeed + this.upSpeed;
        if (this.dy < -10) {
            this.dy = -10;
        } else if (this.dy > 8) {
            this.dy = 8;
        }
        this.y += this.dy;
        if (this.y > 600 - 20) {
            this.y = 600 - 20;
            this.dy = 0;
        } else if (this.y < 0) {
            this.y = 0;
            if (this.flapping === true)
                this.dy = 0;
        }

    }

    flap() {
        this.flapping = true;
        this.maxFlap = this.y - 40;
    }
}
