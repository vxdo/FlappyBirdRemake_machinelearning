class Perceptron {
    constructor() {
        this.w1 = Math.floor(Math.random() * 201) / 100 - 1;
        this.w2 = Math.floor(Math.random() * 201) / 100 - 1;
        this.bias = Math.floor(Math.random() * 201) / 100 - 1;
        this.rate = 0.1;
    }

    train(bird, rect, isTouched) {
        let diff;
        switch (isTouched) {
            case 'u':
                diff = -1;
                break;
            case 'l':
                diff = 1;
                break;
            default:
                diff = 0;
        }
        this.w1 += Math.abs(bird.X - rect.X) * diff * this.rate;
        this.w2 += Math.abs(bird.Y - rect.Y) * diff * this.rate;
        this.bias = diff * this.rate;
        console.log(this.w1, this.w2)
    }

    make_move(x, y) {
        return x * this.w1 + y * this.w2 + this.bias > 0;
    }


}
