let canvas, ctx, fps = 0;
let walls = [];
let interval;
let bird;

function setup(canvas, ctx) {
    let birds = [];
    for (let i = 0; i < 1; i++) {
        birds.push(new Bird(canvas, ctx, "black"))
    }

    let p = new Perceptron();
    document.body.onkeypress = function (e) {
        if (e.key === ' ') {
            flap()
        }
    };
    interval =
        setInterval(function () {

            if (fps % 150 === 0) {
                walls.push(new Wall(canvas, ctx, "black"));
            }

            if (walls.length !== 0) {
                birds.forEach((bird) => {
                    let isTouched = bird.isTouched(walls[0]);
                    p.train(bird, walls[0], isTouched);
                    if (isTouched === 'u' || isTouched === 'l') {
                        fps = 0;
                        walls = walls.slice(1);
                        bird = new Bird(canvas, ctx, "black");
                        p.bird = bird;
                        // clearInterval(interval);
                    }
                })
            }
            fps++;
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            birds.forEach((bird) => {

                if (p.make_move(bird.X, bird.Y))
                    bird.flap();
                bird.update();
                bird.draw();
            })

            walls.forEach(function (w) {
                if (w.x + 20 < 0)
                    walls.splice(w, 1);
                w.draw();
                w.move();

            })
        }, 8.3);
}

function flap() {
    bird.flap()
}
