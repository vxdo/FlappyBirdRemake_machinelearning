class Wall {
    constructor(canvas, ctx, color) {
        this.x = 800;
        this.y = 0;
        this.space = 180;
        this.w = 20;
        this.h = Math.floor(Math.random() * 300) + 20;
        this.h2 = canvas.height - this.h - this.space;
        this.y2 = this.h + this.space;
        this.ctx = ctx;
        this.color = color;
    }

    get X() {
        return this.x;
    }

    get Y() {
        return this.y;
    }

    get Y2() {
        return this.y2;
    }

    get W() {
        return this.w;
    }

    get H() {
        return this.h;
    }

    get H2() {
        return this.h2;
    }

    draw() {
        this.ctx.beginPath();
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x, this.y, this.w, this.h);
        this.ctx.fillStyle = 'red';
        this.ctx.fillRect(this.x, this.y2, this.w, this.h2);
        this.ctx.stroke();
    }

    move() {
        this.x -= 2;
    }

}
